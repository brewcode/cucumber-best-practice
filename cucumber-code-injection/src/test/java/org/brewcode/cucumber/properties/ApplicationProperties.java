package org.brewcode.cucumber.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(value = "application")
public record ApplicationProperties(String name) {
}
