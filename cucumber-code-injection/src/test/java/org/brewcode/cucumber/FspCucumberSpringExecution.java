package org.brewcode.cucumber;

import io.cucumber.spring.CucumberContextConfiguration;
import org.brewcode.cucumber.properties.ApplicationProperties;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;

@EnableAutoConfiguration
@CucumberContextConfiguration
@SpringBootTest(classes = CucumberRunnerTest.class)
@EnableConfigurationProperties(ApplicationProperties.class)
@ComponentScan(FspCucumberSpringExecution.TEST_PACKAGE)
public class FspCucumberSpringExecution {

    static final String TEST_PACKAGE = "org.brewcode.cucumber";
}
