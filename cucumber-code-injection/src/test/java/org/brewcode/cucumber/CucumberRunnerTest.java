package org.brewcode.cucumber;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"pretty"},
        glue = "ru.raiff.test.fsp.fsptest.tests",
        features = "classpath:features"
)
public final class CucumberRunnerTest {
}
